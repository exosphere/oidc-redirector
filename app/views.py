from app import app
from flask import request, redirect
import os

@app.route('/', methods=['POST'])
def home():
    token = request.form['token']
    exosphere_url = os.environ['EXOSPHERE_URL']
    return redirect(exosphere_url + "/auth/oidc-login?token=" + token, code=302)
